// if this shows red, there is a bug in IntelliJ where having a module-info.java makes the dsl show spurious errors. See https://youtrack.jetbrains.net/issue/IDEA-208447
import java.util.Date

plugins {
    `java-library`
    `maven-publish`
    id("com.jfrog.bintray") version "1.8.4"
    id("net.researchgate.release") version "2.8.0"
    id("com.github.ben-manes.versions") version "0.21.0"
}

repositories {
    jcenter()
}

val deps by extra {
    mapOf(
            "junit" to "5.4.0"
    )
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${deps["junit"]}")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.test {
    useJUnitPlatform()
}

tasks.afterReleaseBuild {
    dependsOn(tasks.bintrayUpload)
}

tasks.register<Jar>("sourcesJar") {
    from(sourceSets.main.get().allJava)
    archiveClassifier.set("sources")
}

tasks.register<Jar>("docJar") {
    from(tasks.javadoc)
    archiveClassifier.set("javadoc")
}

publishing {
    publications {
        create<MavenPublication>("bintray") {
            from(components["java"])
            groupId = "org.mpierce.concurrent.throttle"
            artifactId = "task-throttle"
            version = project.version.toString()

            artifact(tasks["sourcesJar"])
            artifact(tasks["docJar"])
        }
    }
}

bintray {
    user = rootProject.findProperty("bintrayUser")?.toString() ?: "FIXME"
    key = rootProject.findProperty("bintrayApiKey")?.toString() ?: "FIXME"
    setPublications("bintray")

    with(pkg) {
        repo = "maven"
        setLicenses("Copyfree")
        vcsUrl = "https://bitbucket.org/marshallpierce/task-throttle"
        name = "org.mpierce.concurrent.throttle:task-throttle"

        with(version) {
            name = project.version.toString()
            released = Date().toString()
            vcsTag = "v" + project.version
        }
    }
}

release {
    tagTemplate = "v\$version"
}
