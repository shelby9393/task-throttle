package org.mpierce.concurrent.throttle;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Supplier;

/**
 * Limits the concurrent execution of cheap units of work to avoid overwhelming other scarce resources.
 *
 * One use case is when you have many network requests to make, but you don't want to make them all at once -- wrap each
 * call in a {@link Supplier} and run them through a TaskThrottle to limit the concurrent requests.
 *
 * @param <T> the result type emitted by the CompletableFutures
 */
public class TaskThrottle<T> {

    // Invariants:
    // currentConcurrency < maxConcurrency -> queue is empty
    // queue is occupied -> currentConcurrency == maxConcurrency
    // currentConcurrency == maxConcurrency implies nothing -- queue could be empty or not

    // guarded by lock
    private final Queue<QueuedTask<T>> queue = new ArrayDeque<>();
    // guarded by lock
    private int currentConcurrency = 0;
    // private lock object so nobody can mess with our synchronization
    private final Object lock = new Object();
    private final int maxConcurrency;

    /**
     * @param maxConcurrency The maximum number of tasks to run in parallel.
     */
    public TaskThrottle(int maxConcurrency) {
        this.maxConcurrency = maxConcurrency;
    }

    /**
     * Provide a Supplier that will start a unit of work (returning a corresponding CompletableFuture) when executed.
     * The work should not be initiated until {@link Supplier#get()} is invoked.
     *
     * Executing the supplier should not block.
     *
     * @param supplier the supplier
     * @return a future that will be completed with the value that the supplier's result completes with
     */
    public CompletableFuture<T> submit(Supplier<CompletionStage<T>> supplier) {
        var future = new CompletableFuture<T>();

        synchronized (lock) {
            if (currentConcurrency < maxConcurrency) {
                runImmediately(supplier, future);
            } else {
                queue.add(new QueuedTask<>(supplier, future));
            }
        }

        return future;
    }

    // guarded by lock
    private void runImmediately(Supplier<CompletionStage<T>> s, CompletableFuture<T> future) {
        currentConcurrency += 1;

        var innerFuture = s.get();

        innerFuture.whenComplete((v, t) -> {
            try {
                if (t != null) {
                    future.completeExceptionally(t);
                } else {
                    future.complete(v);
                }
            } finally {
                synchronized (lock) {
                    currentConcurrency -= 1;

                    if (currentConcurrency < maxConcurrency && !queue.isEmpty()) {
                        QueuedTask<T> q = queue.remove();

                        runImmediately(q.supplier, q.externalFuture);
                    }
                }
            }
        });
    }
}
