package org.mpierce.concurrent.throttle;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;

class TaskThrottleTest {

    @Test
    void allTasksComplete() throws InterruptedException {
        int concurrency = 50;
        var pool = Executors.newFixedThreadPool(concurrency);
        var latch = new CountDownLatch(1);
        var throttle = new TaskThrottle<String>(concurrency);
        var timer = Executors.newSingleThreadScheduledExecutor();
        var futures = new ArrayList<CompletableFuture<String>>();

        int count = 1000;

        for (int i = 0; i < count; i++) {
            int counter = i;
            futures.add(throttle.submit(() -> {
                var future = new CompletableFuture<String>();

                // don't make the supplier block
                pool.submit(() -> {
                    // wait until the test starts
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // simulate a long-ish running task
                    timer.schedule(
                            () -> {
                                // half normal completion, half exceptional
                                if (counter % 2 == 0) {
                                    future.complete("OK");
                                } else {
                                    future.completeExceptionally(new RuntimeException("kaboom"));
                                }
                            },
                            10 + ThreadLocalRandom.current().nextInt(100),
                            TimeUnit.MILLISECONDS);
                });

                return future;
            }));
        }

        // let the task work proceed
        latch.countDown();

        var ok = 0;
        var exception = 0;
        for (CompletableFuture<String> future : futures) {
            try {
                future.get();
                ok += 1;
            } catch (ExecutionException e) {
                exception += 1;
            }
        }

        assertEquals(count / 2, ok);
        assertEquals(count / 2, exception);

        pool.shutdown();
        timer.shutdown();
    }

    @Test
    void concurrencyLimitRespected() throws ExecutionException, InterruptedException {
        var concurrentTasks = new AtomicInteger(0);

        int concurrency = 50;
        var pool = Executors.newFixedThreadPool(concurrency);
        var latch = new CountDownLatch(1);
        var throttle = new TaskThrottle<String>(concurrency);
        var timer = Executors.newSingleThreadScheduledExecutor();
        var futures = new ArrayList<CompletableFuture<String>>();

        int count = 1000;

        for (int i = 0; i < count; i++) {
            futures.add(throttle.submit(() -> {
                var future = new CompletableFuture<String>();

                // don't make the supplier block
                pool.submit(() -> {
                    // wait until the test starts
                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    int currentCount = concurrentTasks.incrementAndGet();

                    // simulate a long-ish running task
                    timer.schedule(() -> {
                                concurrentTasks.decrementAndGet();
                                if (currentCount <= concurrency) {
                                    future.complete("OK " + currentCount);
                                } else {
                                    future.completeExceptionally(new RuntimeException(("Got " + currentCount)));
                                }
                            },
                            10 + ThreadLocalRandom.current().nextInt(100),
                            TimeUnit.MILLISECONDS);
                });

                return future;
            }));
        }

        // let the task work proceed
        latch.countDown();

        for (CompletableFuture<String> future : futures) {
            future.get();
        }

        pool.shutdown();
        timer.shutdown();
    }

    @Test
    void alreadyCompleteFutureWorks() throws ExecutionException, InterruptedException {
        int concurrency = 50;
        var pool = Executors.newFixedThreadPool(concurrency);
        var latch = new CountDownLatch(1);
        var throttle = new TaskThrottle<String>(concurrency);
        var timer = Executors.newSingleThreadScheduledExecutor();
        var futures = new ArrayList<CompletableFuture<String>>();

        int count = 1000;

        for (int i = 0; i < count; i++) {
            futures.add(throttle.submit(() -> CompletableFuture.completedFuture("complete")));
        }

        // let the task work proceed
        latch.countDown();

        for (CompletableFuture<String> future : futures) {
            assertEquals("complete", future.get());
        }

        pool.shutdown();
        timer.shutdown();
    }
}
