[ ![Download](https://api.bintray.com/packages/marshallpierce/maven/org.mpierce.concurrent.throttle%3Atask-throttle/images/download.svg) ](https://bintray.com/marshallpierce/maven/org.mpierce.concurrent.throttle%3Atask-throttle/_latestVersion)

This project exposes one public type, `TaskThrottle`, which lets you control concurrency when you're not using threads as your unit of concurrency (and therefore can't rely on `Semaphore` and friends).

# Example

Consider a situation like a long list of URLs to fetch:

```
List<String> urls = List.of(
    "foo", 
    // ...
    "bar");
```

You could fetch them all with your favorite nonblocking HTTP client:

```
List<CompletableFuture<Response>> futures = urls
    .stream()
    .map((u) -> httpClient.get(u))
    .collect(Collectors.toList());
```

However, that might launch too many requests at once for the server to handle. If you were using a thread for each request, you could use a `Semaphore` to not allow too many requests at once, but that's not appropriate here since presumably nonblocking i/o is running on just one or a couple of threads and we wouldn't want to block them.

Or, you can wrap each request in a `Supplier` that the `TaskThrottle` can invoke:

```
var throttle = new TaskThrottle<Response>(10);

List<CompletableFuture<Response>> futures = urls
    .stream()
    // request won't start until the supplier lambda is invoked
    .map((u) -> throttle.submit(() -> httpClient.get(u)))
    .collect(Collectors.toList());
```

The `TaskThrottle` will only invoke `Supplier#get()` to start a request when there are fewer than 10 (in this case) tasks are running. `TaskThrottle#submit()` returns a `CompletableFuture`, so you can do all your normal futuristic callbacks, etc, with it.
